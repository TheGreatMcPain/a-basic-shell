#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "arraysize.h"
#include "colors.h"
#include "utils.h"

// This function provides the 'lc' builtin command.
//
// This function will return 2 if it want to return to
// the top of the main while loop.
//
// This function will return 1 if it whats to run a command,
// but skip updating the history array.
char *lastCmd(char *history[], char *str, int strSize, int *value) {
  char *args[3];
  char *newCmd = NULL;
  int i;
  int num;
  int counter;
  int selection[MAX_HIS + 1];

  newCmd = (char *)malloc(sizeof(char) * strSize + 1);

  // Make copy if str, so that we don't mess up
  // the original str with strtok.
  strncpy(newCmd, str, strSize + 1);
  // Make sure arrays is actually empty.
  memset(selection, 0, sizeof(selection));
  memset(args, 0, sizeof(args));
  // convert the copy of str to args array.
  strToArgv(args, 3, newCmd);

  // This doesn't actually forkbomb I swear!
  if (strncmp(args[0], "forkbomb", 8) == 0) {
    printf("Starting in...\n");
    for (counter = 5; counter > 0; counter--) {
      printf("%d\n", counter);
      sleep(1);
    }
    printf("\nJust Kidding!\n");

    *value = 2;
  }

  if (strncmp(args[0], "lc", 2) == 0) {
    // List the contents of history.
    if (args[1] == NULL) {
      // Don't do anything if nothing is in history.
      if (strncmp(history[MAX_HIS - 1], "\0", 1) == 0) {
        printf("Nothing has been added to history.\n");
        *value = 2;
        free(newCmd);
        free(str);
        return str;
      }
      newCmd = (char *)realloc(newCmd, strlen(history[MAX_HIS - 1]) + 1);
      strcpy(newCmd, history[MAX_HIS - 1]);
      *value = 1;
      free(str);
      return newCmd;
    } else if (strncmp(args[1], "list", 5) == 0) {
      counter = 1;
      for (i = 0; i < MAX_HIS; i++) {
        if (strncmp(history[i], "\0", 1) != 0) {
          // Label each history string with a number.
          printf("%d: %s\n", counter, history[i]);
          counter++;
        }
      }
      *value = 2;
      free(newCmd);
      free(str);
      return str;
    } else if (isPosInteger(args[1])) {
      if (strlen(args[1]) > 2) {
        printf("Number must be 2 digits or less.\n");
        *value = 2;
        free(newCmd);
        free(str);
        return str;
      }
      sscanf(args[1], "%2d", &num);
      counter = 1;
      // prepare 'selection' so that we can put a number into
      // 'selection', and it will return the "index" of what we want
      // from 'history'.
      for (i = 0; i < MAX_HIS; i++) {
        if (strncmp(history[i], "\0", 1) != 0) {
          selection[counter] = i;
          counter++;
        }
      }
      // Don't attempt to access something from 'history' that
      // doesn't exist.
      if (num >= MAX_HIS + 1 || selection[num] == 0) {
        printf("\"%d\" Doesn't exist in history list.\n\n", num);
        printf("Use 'lc list' to list command history.\n\n");
        *value = 2;
        free(newCmd);
        free(str);
        return str;
      }

      newCmd = (char *)realloc(newCmd, strlen(history[selection[num]]) + 1);
      strcpy(newCmd, history[selection[num]]);
      *value = 1;
      free(str);
      return newCmd;
    } else if (args[1] != NULL) {
      printf("\"%s\" is not a valid option.\n\n", args[1]);
      printf("Use 'lc' to run the previous command.\n\n");
      printf("'lc list' to list command history starting with the latest.\n\n");
      printf("'lc <# from 'lc list'>' to run commands from the history.\n\n");
      *value = 2;
      free(newCmd);
      free(str);
      return str;
    }
  }
  *value = 0;
  free(newCmd);
  return str;
}

// This function is for the 'color' command.
//
// If no other arguments are provided it will simply print
// the current color.
//
// If 'list' is provided it will print the available colors.
//
// If the name of a color is provided it will change the value of
// colorPtr to that color.
void changeColor(char *const args[], struct Color colors[],
                 struct Color *colorPtr1, struct Color *colorPtr2) {
  struct Color *colorTmpPtr = NULL;
  int i;

  // If no arguments print the current color.
  if (args[1] == NULL) {
    printf("Current prompt color is: %s%s\n" RESET, colorPtr1->ansi,
           colorPtr1->name);
    printf("Current directory color is: %s%s\n" RESET, colorPtr2->ansi,
           colorPtr2->name);
    // If 'list' is supplied list the available colors.
  } else if (strncmp(args[1], "list", 4) == 0) {
    for (i = 0; i < NUM_COLORS; i++) {
      printf("%s%s\n", colors[i].ansi, colors[i].name);
    }
    // if 'prompt' is supplied change the 'colorTmpPtr' pointer
    // to colorPtr1. This way when we change the value of 'colorTmpPtr'
    // we also change the value that colorPtr1 points to.
  } else if (strncmp(args[1], "prompt", 6) == 0) {
    colorTmpPtr = colorPtr1;
    // Same as before, but for colorPtr2.
  } else if (strncmp(args[1], "dir", 3) == 0) {
    colorTmpPtr = colorPtr2;
    // Tell the user how to use this command.
  } else if (strncmp(args[1], "\0", 1) != 0) {
    printf("\"%s\" Is not a valid option.\n\n", args[1]);

    printf("Use 'color' to show the currently selected colors.\n\n");
    printf("'color list' to list available colors.\n\n");
    printf("'color prompt <color>' will change the color of the prompt.\n\n");
    printf("'color dir <color>' will change the color of current directory");
    printf("part of the prompt.\n\n");
  }

  if (colorTmpPtr != NULL) {
    if (args[2] == NULL) {
      printf("You need to input a color.\n");
    } else {
      for (i = 0; i < NUM_COLORS; i++) {
        if (strcmp(colors[i].name, args[2]) == 0) {
          *colorTmpPtr = colors[i];
          break;
        }
      }
      if (i == NUM_COLORS) {
        printf("\"%s\" Is not a color.\n", args[2]);
      }
    }
  }
}

// Function for 'cd'
void changeDir(char *const args[]) {
  char *strPtr = NULL;
  char cwd[PATH_MAX];
  char *home = getenv("HOME");
  char newDir[PATH_MAX] = "\0";

  // if 'cd' is not supplied with any arguments
  // just change directory to HOME.
  if (args[1] == NULL) {
    strcpy(newDir, home);
    // if the first character in the directory
    // is a '~' replace '~' with the HOME directory.
  } else if (strncmp(args[1], "~", 1) == 0) {
    strcpy(newDir, home);
    // Ignore the '~' in args[1].
    strPtr = args[1] + 1;
    strcat(newDir, strPtr);
    // If the argument is just a directory with no '~' at
    // the beginning just make the argument 'newDir'.
  } else {
    strcpy(newDir, args[1]);
  }
  // If we succeed in changing directories change the PWD
  // variable to the new directory.
  if (chdir(newDir) == 0) {
    if (getcwd(cwd, sizeof(cwd)) == NULL) {
      printf("getcwd() failed.");
    } else {
      setenv("PWD", cwd, 1);
    }
  } else {
    printf("\"%s\" does not exist.\n", newDir);
  }
}
