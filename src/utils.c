#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "arraysize.h"
#include "colors.h"

// Simply prints the 'prompt'
void genPrompt(struct Color color1, struct Color color2) {
  char *user = getlogin();
  char *home = getenv("HOME");
  char *currentDir = getenv("PWD");
  char newDir[PATH_MAX];
  char hostname[MAX_LINE];

  // Grabs the hosts' hostname.
  gethostname(hostname, MAX_LINE);

  // Remove domain from hostname.
  strcpy(hostname, strtok(hostname, "."));

  // Replace the home directory with '~'.
  if (strncmp(currentDir, home, strlen(home)) == 0) {
    strcpy(newDir, "~");
    // This is setting the starting pointer of currentDir
    // so that we skip the 'home' directory.
    currentDir = currentDir + strlen(home);
    strcat(newDir, currentDir);
  } else {
    // Otherwise just copy currentDir to newDir.
    strcpy(newDir, currentDir);
  }

  // This should print "user@hostname current/directory >"
  // where "user@hostname", and ">" will have their own color, and
  // "current/directory" will have a different color.
  printf("%s%s@%s %s%s%s > " RESET, color1.ansi, user, hostname, color2.ansi,
         newDir, color1.ansi);
}

// Return true if input string is only whitespace.
// otherwise return false.
int isEmpty(char const *str) {
  while (*str != '\0') {
    if (!isspace(*str)) {
      return false;
    }
    str++;
  }
  return true;
}

// Function that is similar to isEmpty, but this
// time it checks if a string is actually an integer.
// Due to how this function works it will return false for
// negative integers.
int isPosInteger(char const *str) {
  while (*str != '\0') {
    if (!isdigit(*str)) {
      return false;
    }
    str++;
  }
  return true;
}

// Moved fork and execvp into it's own function
// that returns the pid from execvp.
// Hopefully this will make 'showpid' easier to
// implement.
//
// Update 02-06-2020: So I learned how to use pipes
// so that the child process can pass information back to
// the parent process. In this case I'm sending the return value
// to the parent which will help the parent know if execvp fails.
int strToArgv(char *argv[], int size, char *str) {
  char tok[3] = " \n";
  char *token = strtok(str, tok);
  int i = 0;
  int x;
  int y;

  while (token != NULL) {
    if (i == size - 1) {
      return 1;
    }
    argv[i] = token;

    token = strtok(NULL, tok);
    i++;
  }

  // If one of the strings from argv have a '\'.
  // Go replace '\' with a space, and append the next
  // string(s). This is to allow 'cd' to work on
  // directories with spaces in their name.
  i = 0;
  while (argv[i] != NULL) {
    y = i;
    while (argv[y][strlen(argv[y]) - 1] == '\\') {
      argv[y][strlen(argv[y]) - 1] = '\0';
      strcat(argv[y], " ");
      strcat(argv[y], argv[y + 1]);
      for (x = y + 1; argv[x] != NULL; x++) {
        argv[x] = argv[x + 1];
      }
    }
    i++;
  }
  return 0;
}

pid_t execArgv(char *const args[]) {
  pid_t pid;
  // Used for return value of execvp.
  int error = 0;
  // Create pipe that will allow child to
  // send the return value of execvp to parent.
  // pipefd[0] is the read end, and
  // pipefd[1] is the write end.
  int pipefd[2];
  if (pipe(pipefd) == -1) {
    printf("Failed to create pipe.\n");
  }

  if ((pid = fork()) == 0) {
    close(pipefd[0]);  // prevent child from writing to read end.
    error = execvp(args[0], args);
    // write execvp's return value to pipe.
    if (write(pipefd[1], &error, sizeof(error)) == -1) {
      printf("Write to pipe failed.\n");
    }
    close(pipefd[1]);  // close pipe when done.
    printf("Error: Command could not be executed\n");
    return -1;
  } else {
    close(pipefd[1]);  // prevent parent from reading the write end.
    // read the value that the child wrote, and put it in 'error'.
    if (read(pipefd[0], &error, sizeof(error)) == -1) {
      printf("Read from pipe failed.\n");
    }
    close(pipefd[0]);  // close pipe when done.
    waitpid(-1, NULL, 0);
  }

  // If execvp fails return 0
  if (error == -1) {
    return 0;
  } else {
    return pid;
  }
}
