/***************************************************************
 * Name of Program: shell
 * Author: James McClain
 * Description: Just a basic shell program that allows the user
 *              to run other terminal programs.
 ***************************************************************/

// use stdbool.h to basically convert 'true' to 1, and 'false' to 0.
// Using limits.h to provide PATH_MAX
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arraysize.h"
#include "builtin.h"
#include "colors.h"
#include "utils.h"

int main() {
  pid_t pid;
  bool success;
  int lc = 0;
  char ch;
  char *cmd = NULL;
  char *cmdCopy = NULL;
  char *myArgv[ARGV_SIZE];
  char *history[MAX_HIS];
  struct Color colors[NUM_COLORS];
  struct Color colorPrompt;
  struct Color colorDir;
  int pids[MAX_PIDS];
  int i;
  int size;

  // There's probably a better way to do this.
  memset(colors, 0, sizeof(colors));
  strcpy(colors[0].ansi, RED);
  strcpy(colors[1].ansi, GREEN);
  strcpy(colors[2].ansi, YELLOW);
  strcpy(colors[3].ansi, BLUE);
  strcpy(colors[4].ansi, MAGENTA);
  strcpy(colors[5].ansi, CYAN);

  strcpy(colors[0].name, "red");
  strcpy(colors[1].name, "green");
  strcpy(colors[2].name, "yellow");
  strcpy(colors[3].name, "blue");
  strcpy(colors[4].name, "magenta");
  strcpy(colors[5].name, "cyan");

  // Set default color.
  // '3' is blue.
  colorPrompt = colors[1];
  colorDir = colors[3];

  memset(pids, 0, sizeof(pids));
  // Initialize history
  for (i = 0; i < MAX_HIS; i++) {
    history[i] = (char *)malloc(sizeof(char) * 1);
    history[i][0] = '\0';
  }

  while (1) {
    success = true;
    cmd = (char *)malloc(MAX_LINE * sizeof(char));
    /* Get User input */
    genPrompt(colorPrompt, colorDir);

    // If the last char of cmd is not a newline (which should be there).
    i = 0;
    size = MAX_LINE;
    while ((ch = fgetc(stdin)) != '\n' && !feof(stdin)) {
      cmd[i] = ch;
      // When i == MAX_LINE, resize cmd.
      if (++i == size) {
        cmd = (char *)realloc(cmd, (i *= 2) * sizeof(char));
        size = i *= 2;
      }
    }
    // Trim unused bytes from cmd
    cmd = (char *)realloc(cmd, (i + 1) * sizeof(char));

    // Add '\0' to prevent overreading cmd.
    cmd[i] = '\0';

    // If no input is provided. (or is just whitespace)
    // go to the beginning of the while loop.
    if (isEmpty(cmd)) {
      continue;
    }
    // Get return value of lastCmd.
    cmd = lastCmd(history, cmd, i, &lc);
    // if 'lc list' was ran just restart the loop.
    if (lc == 2) {
      continue;
    }

    cmdCopy = (char *)malloc(strlen(cmd) + 1);

    // Make a backup of cmd which will later
    // be saved into the history array.
    strcpy(cmdCopy, cmd);

    // clear myArgv.
    memset(myArgv, 0, sizeof(myArgv));

    /* Convert input to array of strings */
    if (strToArgv(myArgv, ARGV_SIZE, cmd) == 1) {
      printf("Input must be less than %d arguments.\n", ARGV_SIZE);
      continue;
    }

    /* Other Build-in commands */
    if (strncmp(myArgv[0], "exit", 4) == 0) {
      printf("Exiting!\n");
      break;
    } else if (strncmp(myArgv[0], "color", 5) == 0) {
      changeColor(myArgv, colors, &colorPrompt, &colorDir);
    } else if (strncmp(myArgv[0], "cd", 2) == 0) {
      changeDir(myArgv);
    } else if (strncmp(myArgv[0], "showpid", 7) == 0) {
      // skim through the pid array, and if it's
      // not 0 print it.
      for (i = 0; i < MAX_PIDS; i++) {
        if (pids[i] != 0) {
          printf("%d\n", pids[i]);
        }
      }
    } else {
      // Run non-builtin, and store pid in pid array.
      pid = execArgv(myArgv);
      // if execArgv returns a -1 exit the loop.
      // This is so that the child process can free memory.
      if (pid == -1) {
        break;
      }
      // if execArgv returns 0 don't add it to pid array.
      if (pid != 0) {
        for (i = 1; i < MAX_PIDS; i++) {
          pids[i - 1] = pids[i];
        }
        pids[MAX_PIDS - 1] = pid;
      }
      // If execArgv returns 0 change 'success' is false.
      if (pid == 0) {
        success = false;
      }
    }

    // If 'lc', or 'lc #', was ran don't add the copy of cmd
    // to history.
    if (lc != 1) {
      // If success is true add copy of cmd to history.
      if (success == true) {
        for (i = 1; i < MAX_HIS; i++) {
          free(history[i - 1]);
          history[i - 1] = (char *)malloc(strlen(history[i]) + 1);
          strcpy(history[i - 1], history[i]);
        }
        free(history[MAX_HIS - 1]);
        history[MAX_HIS - 1] = (char *)malloc(strlen(cmdCopy) + 1);
        strcpy(history[MAX_HIS - 1], cmdCopy);
      }
    }
    free(cmd);
    free(cmdCopy);
  }
  // Deinitialize history
  for (i = 0; i < MAX_HIS; i++) {
    free(history[i]);
  }

  free(cmd);
  free(cmdCopy);
  return 0;
}
