#ifndef BUILTIN_H
#define BUILTIN_H

#include "colors.h"

char *lastCmd(char *history[], char *str, int strSize, int *value);

void changeColor(char *const args[], struct Color colors[],
                 struct Color *colorPtr1, struct Color *colorPtr2);

void changeDir(char *const args[]);

#endif
