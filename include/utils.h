#ifndef UTILS_H
#define UTILS_H

#include <sys/types.h>

void genPrompt(struct Color color1, struct Color color2);

int isEmpty(char const *str);

int isPosInteger(char const *str);

int strToArgv(char *argv[], int size, char *str);

pid_t execArgv(char *const args[]);

#endif
